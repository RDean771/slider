const slike=['./assets/img/slider-image-7.jpg',
             './assets/img/slider-image-6.jpg',
             './assets/img/slider-image-9.jpg',
             './assets/img/slider-image-8.jpg',
             './assets/img/slider-image-5.jpg',
             './assets/img/slider-image-4.jpg',
             './assets/img/slider-image-1.jpg',
             './assets/img/slider-image-2.jpg',
             './assets/img/slider-image-3.jpg'
            ];
            
function ucitajSlike(){
    for(let i = 0; i < slike.length; i++){
        const img=document.createElement('img');
        img.setAttribute('src',slike[i]);
        if(i != 4 || i != 8){
            img.setAttribute('id','slika' + i.toString());
            img.setAttribute('class','mr-2');
        }
        (i < Math.ceil(slike.length/2))? document.getElementById('gornjeslike').appendChild(img) : document.getElementById('donjeslike').appendChild(img);
    }
}
function desniPomak(){
    const zadnji=slike.pop();
    slike.splice(0,0,zadnji);
    for(let i = 0; i < slike.length; i++){
        document.getElementById('slika' + i.toString()).setAttribute('src',slike[i]);
    }
}

function lijeviPomak(){
    const prvi=slike.shift();
    slike.push(prvi);
    for(let i = 0; i < slike.length; i++){
        document.getElementById('slika' + i.toString()).setAttribute('src',slike[i]);
    }
}
